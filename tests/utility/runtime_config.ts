import fs from "fs/promises"

export default class RuntimeConfig {
    testDirectory: string;

    constructor(){
        var json = fs.readFile("./app_config.json").toString()
        var configs = JSON.parse(json)
        this.testDirectory = configs.testDirectory
    }
}