import { ResourceId } from "cheapspeech-shared"
import axios from "axios"
import fs from "fs"
import FormData from "form-data";

export default class CreateFilesOnShard {
    address: string;
    port: string;
    resourceId: ResourceId = new ResourceId()

    constructor(address: string, port: string) {
        this.address = address
        this.port = port
    }

    async createFiles(numFiles: number) {
        for (var i = 0; i < numFiles; i++) {
            var file = fs.createReadStream("./test.txt")
            var id = this.resourceId.getNewId()
            var name = id + ".txt"

            var url = `http://${this.address}:${this.port}/file?fileName=${name}`

            try {
                var formData = new FormData()
                formData.append("data", file)
                var response = await axios.post(url, formData, {
                    headers: formData.getHeaders()
                })
                console.log(response.data)
            }
            catch (err) {
                console.log("Failed to upload test file.")
            }

        }
    }
}

function main() {
    var fileCreator = new CreateFilesOnShard("127.0.0.2", "8001")
    fileCreator.createFiles(500)
}

main()