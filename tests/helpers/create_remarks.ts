import { ResourceId } from "cheapspeech-shared"
import axios from "axios"
import fs from "fs"

export default class CreateRemarks {
    address: string;
    port: string;
    resourceId: ResourceId = new ResourceId()
    sessionToken: string;

    constructor(address: string, port: string, sessionToken: string) {
        this.address = address
        this.port = port
        this.sessionToken = sessionToken
    }

    async createRemarks(numRemarks: number) {
        var testRemark = JSON.stringify(
            {
                id: this.resourceId.getNewId(),
                title: "Test Title",
                text: "Test Text",
                tags: ["test", "tags"],
                type: "text"
            }
        )

        for (var i = 0; i < numRemarks; i++) {
            var url = `http://${this.address}:${this.port}/remark`

            try {

                var response = await axios.post(url, testRemark, {
                    headers: {
                        "session-token": this.sessionToken,
                        "content-type": "application/json"
                    }
                })
                console.log(response.data)
            }
            catch (err) {
                console.log("Failed to create test remark.")
            }

        }
    }
}

function main() {
    var remarkCreator = new CreateRemarks("127.0.0.1", "8000", "G8t7mwH0QxrIZ0JbCKA49snzqiywuWTQ0TxS5ZXmM3mqHiT9nA==")
    remarkCreator.createRemarks(15000)
}

main()